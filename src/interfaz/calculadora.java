package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.SystemColor;

public class calculadora {

	private JFrame frame;
	private JTextField TextResultado;
	private JTextField TextOperaciones;
	private float resultado;
	private JTextField textResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					calculadora window = new calculadora();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public calculadora() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(102, 205, 170));
		frame.setBounds(300, 100, 417, 528);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		TextResultado = new JTextField();
		TextResultado.setHorizontalAlignment(SwingConstants.RIGHT);
		TextResultado.setFont(new Font("Tahoma", Font.BOLD, 44));
		TextResultado.setText("0");
		TextResultado.setBounds(12, 36, 377, 60);
		TextResultado.setColumns(10);
		
		JButton btMC = new JButton("MC");
		btMC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btMC.setBackground(Color.LIGHT_GRAY);
		btMC.setBounds(12, 109, 66, 49);
		frame.getContentPane().add(btMC);
		
		JButton btMn = new JButton("M-");
		btMn.setBackground(Color.LIGHT_GRAY);
		btMn.setBounds(324, 109, 66, 49);
		frame.getContentPane().add(btMn);
		
		JButton btMR = new JButton("MR");
		btMR.setBackground(Color.LIGHT_GRAY);
		btMR.setBounds(90, 109, 66, 49);
		frame.getContentPane().add(btMR);
		
		JButton btMp = new JButton("M+");
		btMp.setBackground(Color.LIGHT_GRAY);
		btMp.setBounds(246, 109, 66, 49);
		frame.getContentPane().add(btMp);
		
		JButton btMS = new JButton("MS");
		btMS.setBackground(Color.LIGHT_GRAY);
		btMS.setBounds(168, 109, 66, 49);
		frame.getContentPane().add(btMS);
		
		JButton bteliminar = new JButton("\u2190");
		bteliminar.setFont(new Font("Tahoma", Font.BOLD, 30));
		bteliminar.setBackground(Color.LIGHT_GRAY);
		bteliminar.setBounds(12, 171, 66, 49);
		frame.getContentPane().add(bteliminar);
		
		JButton btRaiz = new JButton("\u263A");
		btRaiz.setBackground(Color.LIGHT_GRAY);
		btRaiz.setBounds(324, 171, 66, 49);
		frame.getContentPane().add(btRaiz);
		
		JButton btMasMenos = new JButton("\u00B1");
		btMasMenos.setBackground(Color.LIGHT_GRAY);
		btMasMenos.setBounds(246, 171, 66, 49);
		frame.getContentPane().add(btMasMenos);
		
		JButton btC = new JButton("C");
		btC.setBackground(Color.LIGHT_GRAY);
		btC.setBounds(168, 171, 66, 49);
		frame.getContentPane().add(btC);
		
		JButton btCE = new JButton("CE");
		btCE.setBackground(Color.LIGHT_GRAY);
		btCE.setBounds(90, 171, 66, 49);
		frame.getContentPane().add(btCE);
		
		JButton bt7 = new JButton("7");
		bt7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"7");
			}
		});
		bt7.setBackground(Color.LIGHT_GRAY);
		bt7.setBounds(11, 233, 66, 49);
		frame.getContentPane().add(bt7);
		
		JButton btPorcentaje = new JButton("%");
		btPorcentaje.setBackground(Color.LIGHT_GRAY);
		btPorcentaje.setBounds(323, 233, 66, 49);
		frame.getContentPane().add(btPorcentaje);
		
		JButton btDividir = new JButton("/");
		btDividir.setBackground(new Color(0, 255, 127));
		btDividir.setBounds(245, 233, 66, 49);
		frame.getContentPane().add(btDividir);
		
		JButton bt9 = new JButton("9");
		bt9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"9");
			}
		});
		bt9.setBackground(Color.LIGHT_GRAY);
		bt9.setBounds(167, 233, 66, 49);
		frame.getContentPane().add(bt9);
		
		JButton bt8 = new JButton("8");
		bt8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"8");
			}
		});
		bt8.setBackground(Color.LIGHT_GRAY);
		bt8.setBounds(89, 233, 66, 49);
		frame.getContentPane().add(bt8);
		
		JButton bt4 = new JButton("4");
		bt4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"4");
			}
		});
		bt4.setBackground(Color.LIGHT_GRAY);
		bt4.setBounds(11, 295, 66, 49);
		frame.getContentPane().add(bt4);
		
		JButton btFraccion = new JButton("1/x");
		btFraccion.setBackground(Color.LIGHT_GRAY);
		btFraccion.setBounds(323, 295, 66, 49);
		frame.getContentPane().add(btFraccion);
		
		JButton btMultiplicar = new JButton("*");
		btMultiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btMultiplicar.setBackground(new Color(255, 228, 181));
		btMultiplicar.setBounds(245, 295, 66, 49);
		frame.getContentPane().add(btMultiplicar);
		
		JButton bt6 = new JButton("6");
		bt6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"6");
			}
		});
		bt6.setBackground(Color.LIGHT_GRAY);
		bt6.setBounds(167, 295, 66, 49);
		frame.getContentPane().add(bt6);
		
		JButton bt5 = new JButton("5");
		bt5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"5");
			}
		});
		bt5.setBackground(Color.LIGHT_GRAY);
		bt5.setBounds(89, 295, 66, 49);
		frame.getContentPane().add(bt5);
		
		JButton bt1 = new JButton("1");
		bt1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"1");
			}
		});
		bt1.setBackground(Color.LIGHT_GRAY);
		bt1.setBounds(12, 357, 66, 49);
		frame.getContentPane().add(bt1);
		
		JButton btRestar = new JButton("-");
		btRestar.setBackground(SystemColor.info);
		btRestar.setBounds(246, 357, 66, 49);
		frame.getContentPane().add(btRestar);
		
		JButton bt3 = new JButton("3");
		bt3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"3");
			}
		});
		bt3.setBackground(Color.LIGHT_GRAY);
		bt3.setBounds(168, 357, 66, 49);
		frame.getContentPane().add(bt3);
		
		JButton bt2 = new JButton("2");
		bt2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"2");
			}
		});
		bt2.setBackground(Color.LIGHT_GRAY);
		bt2.setBounds(90, 357, 66, 49);
		frame.getContentPane().add(bt2);
		
		JButton btSumar = new JButton("+");
		btSumar.setBackground(SystemColor.desktop);
		btSumar.setBounds(246, 417, 66, 49);
		frame.getContentPane().add(btSumar);
		
		JButton btcoma = new JButton(",");
		btcoma.setBackground(Color.LIGHT_GRAY);
		btcoma.setBounds(168, 417, 66, 49);
		frame.getContentPane().add(btcoma);
		
		JButton bt0 = new JButton("0");
		bt0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TextOperaciones.setText(TextOperaciones.getText()+"0");
			}
		});
		bt0.setBackground(Color.LIGHT_GRAY);
		bt0.setBounds(12, 417, 144, 49);
		frame.getContentPane().add(bt0);
		
		JButton btIgual = new JButton("=");
		btIgual.setFont(new Font("Tahoma", Font.BOLD, 30));
		btIgual.setBackground(Color.LIGHT_GRAY);
		btIgual.setBounds(324, 357, 66, 109);
		frame.getContentPane().add(btIgual);
		
		TextOperaciones = new JTextField();
		TextOperaciones.setHorizontalAlignment(SwingConstants.RIGHT);
		TextOperaciones.setFont(new Font("Tahoma", Font.BOLD, 10));
		TextOperaciones.setColumns(10);
		TextOperaciones.setBounds(12, 11, 377, 24);
		frame.getContentPane().add(TextOperaciones);
		
		textResultado = new JTextField();
		textResultado.setHorizontalAlignment(SwingConstants.RIGHT);
		textResultado.setFont(new Font("Tahoma", Font.BOLD, 44));
		textResultado.setColumns(10);
		textResultado.setBounds(12, 33, 377, 50);
		frame.getContentPane().add(textResultado);
	}
}
