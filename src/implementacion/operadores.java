package implementacion;

public class operadores {
	
	private double resultado;
	private double terminoB;
	private String ecuacion;
	
	public operadores() 
	{
		resultado = 0;
		terminoB  = 0;
		ecuacion = "";
	}
	
public double getResultado()
{
	return resultado;
}

public double getTerminoB()
{
	return terminoB;
}

public String getEcuacion()
{
	return ecuacion;
}

public void setTerminoB(double a)
{
	terminoB = a;
}

public void resetTerminoB()
{
	terminoB = 0;
}

public void resetResultado()
{
	resultado = 0;
}

public void resetEcuacion()
{
	ecuacion = "";
}

public void suma (double a , double b) 
{
	resultado = a+b;
}

public void resta (double a , double b) 
{
	resultado = a-b;
}

public void multiplicacion (double a , double b) 
{
	resultado = a*b;

}

public void division (double a , double b) 
{
	resultado = a/b;

}

public void elevar (int a) 
{	
	if (a==0)
	{
		resultado = 1;
	}
	
	else if (a==2)
	{
		double provisorio = resultado;
		for ( int i = 2; i < a;i++) 
		{
			resultado= resultado + provisorio;
		}
	}
}

public void raiz ()
{
	resultado = Math.sqrt(resultado);
}

public void concatenarEcuacion(char i)
{
	ecuacion+= i;
}

}
